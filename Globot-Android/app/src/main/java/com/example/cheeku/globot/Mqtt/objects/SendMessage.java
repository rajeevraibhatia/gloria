package com.example.cheeku.globot.Mqtt.objects;

/**
 * Created by deepti.arya on 4/27/2016.
 * Json
 * Send Message for any group
 Topic : <groupId>
 Request :
 {'groupId':'group1user1','message':'Aryan','userId':'user1','time':'1234','globot':1,'state':'0'}

 Response :

 { 'state' : '16','groupId':'group2user2','message':'Hello aryan, how are you tod
 ay?','time':'1461994224973','globot':0,'userId':'globot'}
 */
public class SendMessage {
    public boolean left;
    public String userId;
    public String msg;
    public Long timeMilli;
    public int globot;
    public String state;
    public String groupId;

    public SendMessage(boolean left, String groupId, String msg,String userId, Long time, int globot, String state)
    {
        //this.time=time;
        timeMilli = time;
        this.userId=userId;
        this.msg = msg;
       // this.left = left;
        this.globot = globot;
        this.state = state;
        this.groupId = groupId;
    }

    public SendMessage(MessagesSubscribeTopic msg)
    {
        //this.time=time;
        timeMilli = msg.time;
        this.userId=msg.userId;
        this.msg = msg.messagePayload;
        //this.left = left;
        //this.globot = globot;
       // this.state = state;
       // this.groupId = msg.groupId;
    }

    public SendMessage() {

    }
}

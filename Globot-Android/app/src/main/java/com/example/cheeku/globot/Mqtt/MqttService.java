package com.example.cheeku.globot.Mqtt;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.example.cheeku.globot.Constants;
import com.example.cheeku.globot.Mqtt.objects.JsonParser;
import com.example.cheeku.globot.Mqtt.objects.LaunchPayloadResponse;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.UUID;


public class MqttService extends Service implements MqttCallback {
    LocalBroadcastManager broadcaster;
    Context context;


    MqttJava mqttJava = new MqttJava();
    JsonParser parse = new JsonParser(context);
    MqttClient client;
    LaunchPayloadResponse launchPayloadResponse;
    public void connect(Context context) {
        try {
            this.context = context;
//            broadcaster = LocalBroadcastManager.getInstance(getApplicationContext());
            MemoryPersistence persistence = new MemoryPersistence();
            String clientId = UUID.randomUUID().toString();
            client = new MqttClient("tcp://13.76.138.197:1883",clientId, persistence);
            MqttConnectOptions options = new MqttConnectOptions();
            options.setConnectionTimeout(60*60*60);
            client.connect(options);
            client.setCallback(this);
            mqttJava.setClient(client);

        } catch (MqttException e) {
            e.printStackTrace();
        }
       }

    public void subscribe(String subs)
    {
        try {
            client = mqttJava.getClient();
            if(client == null)
            {
                connect(context);
            }

            client.subscribe(subs, 0);
            Constants.isSubscribed = true;
            //  client.subscribe(subs);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void sendStringMessage(String subs, String msg)
    {
        try {

            MqttMessage message = new MqttMessage();
            message.setPayload(msg.getBytes());
            message.setQos(0);
            //message.setRetained(false);
            client.publish(subs,message);
            // client.publish(subs, message);
        }
        catch (MqttException e)
        {
            e.printStackTrace();
        }
    }

    public void backPressed()
    {
        try {
            client.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
    public void sendJSONMessage(String subs, String msg)
    {
        try {
            MqttMessage message = new MqttMessage();
            message.setPayload(msg.getBytes());
            message.setRetained(false);
            client.publish(subs, message);
        }
        catch (MqttException e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public void connectionLost(Throwable cause) {
        String a = ";";
        connect(context);
        subscribe(mqttJava.getTopic());
    }
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        System.out.print(message.toString());
        if(!Constants.isSubscribed) {
            Constants.isSubscribed = true;
            //Parse appLaunched response
            if (topic.toString().equals("appLaunched")) {
                parse.launchResponseJSONParser(message.toString());
            }
            //Parse response of create group
            else if(topic.toString().equals("createGroup"))
            {

            }
            //Parse sendMessage Response
            else
            {
                parse.sendMessageResponse(message.toString());
                sendResult();
            }
        }
        else {
            Constants.isSubscribed = false;
        }
        //  main.processResponse(message.getPayload().toString(), false);
    }
    public void sendResult() {
        Intent intent = new Intent(Constants.MQTT_MSG);
        broadcaster.sendBroadcast(intent);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //return super.onStartCommand(intent, flags, startId);
        parse = new JsonParser(context);
        connect(context);
        subscribe("appLaunched");
        sendStringMessage("appLaunched", "{'userId': 'user1'}");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

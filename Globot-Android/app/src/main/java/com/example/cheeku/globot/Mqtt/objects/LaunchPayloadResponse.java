package com.example.cheeku.globot.Mqtt.objects;

import java.util.ArrayList;

/**
 * Created by deepti.arya on 4/19/2016.
 */
public class LaunchPayloadResponse {
   public ArrayList<String> userID;//all the users
    public ArrayList<GroupSubscribed> groupSubscribeds;//will have evrything except messages
}

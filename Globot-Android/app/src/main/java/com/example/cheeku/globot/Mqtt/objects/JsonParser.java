package com.example.cheeku.globot.Mqtt.objects;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.example.cheeku.globot.Constants;
import com.example.cheeku.globot.Mqtt.MqttJava;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by deepti.arya on 4/19/2016.
 *
 * {
 *     UserIds:["fgdf","gvdf","dfg"]
 *     GroupSubscribed :
 *     [
 *     {
 *     GroupName : "grpname1"
 *     GroupMessages : [{"msg1"},{"msg2"},{"msg3"} ]
 *     }
 *     ,{
 *     GroupName : "grpname2"
 *     GroupMessages : [{"msg1"},{"msg2"},{"msg3"} ]
 *     }
 *     ]
 *
 * }
 */
public class JsonParser {

    Context context;
    LocalBroadcastManager broadcaster;
    public JsonParser(Context context)
    {
        this.context = context;
        broadcaster = LocalBroadcastManager.getInstance(context);
    }
    public void sendResult(String type) {
        Intent intent = new Intent(Constants.MQTT_MSG);
        intent.putExtra("Type", type);
        broadcaster.sendBroadcast(intent);
    }

    static LaunchPayloadResponse launchPayloadResponse;
    ArrayList<GroupSubscribed> groupSubscribed;


    static SendMessage messageResponse;


    static ArrayList<String> participants;
    Constants cons = new Constants();
    public LaunchPayloadResponse launchResponseJSONParser(String response) {

        launchPayloadResponse = new LaunchPayloadResponse();
        groupSubscribed = new ArrayList<GroupSubscribed>();
        ArrayList<String> userids = new ArrayList<String>();
        ArrayList<String> members = new ArrayList<String>();
        try {//all the userids
            JSONObject jsonObj = new JSONObject(response);
            JSONArray userIDS = jsonObj.getJSONArray("userIds");
            for (int i = 0; i < userIDS.length(); i++) {
                userids.add(userIDS.getString(i));
            }
            //all the grpsubscribed and msgs
            JSONArray grpsubs = jsonObj.getJSONArray("groups");
            for (int j = 0; j < grpsubs.length(); j++) {
                GroupSubscribed grpsubsobj = new GroupSubscribed();
                JSONObject oneObject = grpsubs.getJSONObject(j);
                grpsubsobj.groupId = oneObject.getString("groupId");
                grpsubsobj.groupName = oneObject.getString("groupName");
                JSONArray mems = oneObject.getJSONArray("members");
                for (int i = 0; i < mems.length(); i++) {
                    members.add(mems.getString(i));
                }
                grpsubsobj.groupMembers = members;
                grpsubsobj.groupOwner = oneObject.getString("groupOwner");
                grpsubsobj.status = oneObject.getBoolean("status");
                groupSubscribed.add(grpsubsobj);
                members = new ArrayList<String>();
            }
//now object of complete response
            launchPayloadResponse.userID = userids;
            launchPayloadResponse.groupSubscribeds = groupSubscribed;
            setLaunchPayloadResponse(launchPayloadResponse);
            for(GroupSubscribed grp:launchPayloadResponse.groupSubscribeds)
            {
                if(grp.groupId.toString().equals(cons.getGroupId(context))){
                    setSelectedGrp(grp);
                    //cons.setGroupId(JsonParser.getSelectedGrp().groupId,context);
                }
            }
            Constants.isSubscribed = false;
            sendResult("appLaunched");
        } catch (JSONException e) {
            Constants.isSubscribed = true;
            e.printStackTrace();
        }

        return launchPayloadResponse;
    }

    public static LaunchPayloadResponse getLaunchPayloadResponse() {
        return launchPayloadResponse;
    }

    public void setLaunchPayloadResponse(LaunchPayloadResponse launchPayloadResponse) {
        this.launchPayloadResponse = launchPayloadResponse;
    }

    public ArrayList<String> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<String> participants) {
        this.participants = participants;
    }

    public static GroupSubscribed selectedGrp;
    public static void setSelectedGrp(GroupSubscribed selectedGrp) {
        JsonParser.selectedGrp = selectedGrp;
    }

    public static GroupSubscribed getSelectedGrp() {
        return selectedGrp;
    }

    /*{ 'state' : '16','groupId':'group2user2','message':'Hello aryan, how are you tod
    ay?','time':'1461994224973','globot':0,'userId':'globot'}
    */
    public SendMessage sendMessageResponse(String response) {
        messageResponse = new SendMessage();
        String date1 ="";
        try {
            JSONObject parentObj = new JSONObject(response);
            messageResponse.left = false;
            messageResponse.state = parentObj.getString("state");
            messageResponse.groupId = parentObj.getString("groupId");
            messageResponse.msg = parentObj.getString("message");
            date1 = parentObj.getString("time");
            messageResponse.timeMilli = parseDate(date1);
            messageResponse.globot = parentObj.getInt("globot");
            messageResponse.userId = parentObj.getString("userId");
            setMessageResponse(messageResponse);
            sendResult("send_message");
            MqttJava.globotNotification(messageResponse.msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return messageResponse;
    }

    public static SendMessage getMessageResponse() {
        return messageResponse;
    }

    public static void setMessageResponse(SendMessage messageResponse) {
        JsonParser.messageResponse = messageResponse;
    }

    public static String getState()
    {
        if(getMessageResponse()!=null && getMessageResponse().state!=null)
            return getMessageResponse().state;
        else
            return "0";
    }

    public Long parseDate(String date1)
    {
        long millisecond = Long.parseLong(date1);
        //String dateString= DateFormat.format("MM/dd/yyyy", new Date(millisecond)).toString();
        // Date d = parseDate(dateString);
        //  Calendar c = Calendar.getInstance();
        return millisecond;
    }

    public static Long getCurrentTime()
    {
        Calendar c = Calendar.getInstance();
        return c.getTimeInMillis();
    }

    public static SubscribeGroupResponse getCasandraMsgs() {
        return casandraMsgs;
    }

    public static void setCasandraMsgs(SubscribeGroupResponse casandraMsgs) {
        JsonParser.casandraMsgs = casandraMsgs;
    }

    public static SubscribeGroupResponse casandraMsgs;
    /*
    * "id":0,
            "groupId":"group1user1",
            "messagePayload":"Hello aryan, how are you today?",
            "userId":"globot",
            "time":1461996240938*/
    public SubscribeGroupResponse getAllCasandraMessages(String response) {
        SubscribeGroupResponse grpMsgs = new SubscribeGroupResponse();
        ArrayList<MessagesSubscribeTopic> list = new ArrayList<MessagesSubscribeTopic>();
        MessagesSubscribeTopic msgs;
        try {
            JSONObject parentObj = new JSONObject(response);
            JSONArray msgArray = parentObj.getJSONArray("messages");
            for(int i =0; i< msgArray.length();i++)
            {
                msgs = new MessagesSubscribeTopic();
                JSONObject oneObject = msgArray.getJSONObject(i);
                msgs.groupId = oneObject.getString("groupId");
                msgs.messagePayload = oneObject.getString("messagePayload");
                msgs.userId = oneObject.getString("userId");
                msgs.time = oneObject.getLong("time");
                list.add(msgs);
            }
            sendResult("subscribeTopic");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        grpMsgs.messages = list;
        setCasandraMsgs(grpMsgs);
        return grpMsgs;
    }
    /*
    * {"groupId":"group3user1","groupName":"group3","members":["user1","user2"],"group
    Owner":"user1","status":false}*/
    public void creaetGrp(String response)
    {

        try {
            JSONObject parentObj = new JSONObject(response);

            boolean check = parentObj.getBoolean("status");
            sendResult("createGroup");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
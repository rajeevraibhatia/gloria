package com.example.cheeku.globot.alarm;

import android.app.AlarmManager;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.cheeku.globot.R;

import java.util.Calendar;

public class AlarmActivity extends DialogFragment {

    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private TimePicker alarmTimePicker;
    private static AlarmActivity inst;
    private TextView alarmTextView;

    public static AlarmActivity instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_alarm, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        alarmTimePicker = (TimePicker) rootView.findViewById(R.id.alarmTimePicker);
        alarmTextView = (TextView) rootView.findViewById(R.id.alarmText);
        Button alarmToggle = (Button) rootView.findViewById(R.id.alarmToggle);
        alarmToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MyActivity", "Alarm On");
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker.getCurrentHour());
                calendar.set(Calendar.MINUTE, alarmTimePicker.getCurrentMinute());
                Intent myIntent = new Intent(getActivity(), AlarmReceiver.class);
                pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, myIntent, 0);
                alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
                dismiss();
            }
        });
        alarmManager = (AlarmManager) getActivity().getSystemService(getActivity().ALARM_SERVICE);
        return rootView;
    }

    public void onToggleClicked(View view) {
        //  if (((ToggleButton) view).isChecked()) {

    } /*else {
            alarmManager.cancel(pendingIntent);
            setAlarmText("");
            Log.d("MyActivity", "Alarm Off");
        }*/
    //}

    public void setAlarmText(String alarmText) {
        alarmTextView.setText(alarmText);
    }
}
package com.example.cheeku.globot;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cheeku.globot.Mqtt.MqttJava;
import com.example.cheeku.globot.Mqtt.objects.CreatePayload;
import com.example.cheeku.globot.Mqtt.objects.JsonParser;
import com.example.cheeku.globot.Mqtt.objects.MessagesSubscribeTopic;
import com.example.cheeku.globot.Mqtt.objects.SendMessage;
import com.example.cheeku.globot.adapters.MessageAdapter;
import com.example.cheeku.globot.alarm.AlarmActivity;
import com.example.cheeku.globot.custom_central.RBLControls;
import com.example.cheeku.globot.custom_central.RBLService;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import de.svenjacobs.loremipsum.LoremIpsum;

public class MainActivity extends AppCompatActivity {
    // private DiscussArrayAdapter adapter;
    private MessageAdapter adapter;
    private ListView lv;
    private EditText editText1;
    private TextView grpName;
    FloatingActionButton fabanim, fabenter, ble;
    public static Boolean isTextModeOn = false;
    BroadcastReceiver receiver, receiver1;
    private LinkedList<ProgressType> mProgressTypes;
    //TTS
    private LoremIpsum ipsum;
    private Speaker speaker;
    RBLControls rblControls;
    RequestResponseProcessing reqrest;
    private FloatingActionMenu menuRed;
    private Handler mUiHandler = new Handler();
    private FloatingActionButton fab1;
    private FloatingActionButton fab2;
    private FloatingActionButton fab3;
    private FloatingActionButton fab4;
    private FloatingActionButton fab5;
    Handler handler;
    private List<FloatingActionMenu> menus = new ArrayList<>();
    MqttJava mqttJava = new MqttJava();
    //MqttService mqttService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        handler = new Handler();
        ipsum = new LoremIpsum();
        if (JsonParser.getSelectedGrp() != null) {
            mqttJava.subscribe("subscribeTopic");
            mqttJava.sendStringMessage("subscribeTopic", "{'groupId':'" + JsonParser.getSelectedGrp().groupId + "'}");
        }
        Constants.Globot = 1;
        mProgressTypes = new LinkedList<>();
        for (ProgressType type : ProgressType.values()) {
            mProgressTypes.offer(type);
        }
        rblControls = new RBLControls(this);
        reqrest = new RequestResponseProcessing();
        speaker = new Speaker(this);
        speaker.allow(true);
        grpName = (TextView) findViewById(R.id.grp_name);

        menuRed = (FloatingActionMenu) findViewById(R.id.menu_red);
        fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab3 = (FloatingActionButton) findViewById(R.id.fab3);
        fab4 = (FloatingActionButton) findViewById(R.id.fab4);
        fab5 = (FloatingActionButton) findViewById(R.id.fab5);
        menuRed.setClosedOnTouchOutside(true);
        menuRed.setVisibility(View.GONE);
        menus.add(menuRed);
        fab1.setOnClickListener(clickListener);
        fab2.setOnClickListener(clickListener);
        fab3.setOnClickListener(clickListener);
        fab4.setOnClickListener(clickListener);
        fab5.setOnClickListener(clickListener);

        int delay = 400;
        for (final FloatingActionMenu menu : menus) {
            mUiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    menu.showMenuButton(true);
                }
            }, delay);
            delay += 150;
        }


        menuRed.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuRed.isOpened()) {
                }

                menuRed.toggle(true);
            }
        });

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String response = intent.getStringExtra(Constants.COPA_MESSAGE);
                Boolean isRequest = intent.getBooleanExtra(Constants.COPA_STATE, true);
                SendMessage msg;
                if(isRequest)
                {
                    int i =0;
                    if(Constants.InList || Constants.IsWeather)
                    {
                        i=0;
                    }
                    else
                    {
                        i=Constants.Globot;
                    }

                    msg = new SendMessage(isRequest, MqttJava.getTopic(), response, Constants.USERNAME, JsonParser.getCurrentTime(), i, JsonParser.getState());

                }
                else if(Constants.IsWeather)
                {
                    String a = processResponse(response, isRequest);
                    if (a == null) {
                        // msg = new SendMessage(isRequest, MqttJava.getTopic(), response, "gloria", JsonParser.getCurrentTime(), Constants.Globot, JsonParser.getState());

                        msg=null;
                    } else {
                        Constants.InList = false;
                        msg = new SendMessage(isRequest, MqttJava.getTopic(), a, "gloria", JsonParser.getCurrentTime(), 0, JsonParser.getState());


                    }
                }
                else
                {


                    String a = processResponse(response, isRequest);

                    if (a == null) {
                        msg = new SendMessage(isRequest, MqttJava.getTopic(), response, "gloria", JsonParser.getCurrentTime(), Constants.Globot, JsonParser.getState());

                    } else {
                        Constants.InList = false;
                        msg = new SendMessage(isRequest, MqttJava.getTopic(), a, "gloria", JsonParser.getCurrentTime(), 0, JsonParser.getState());


                    }
                    //adapter.add(msg);
                    //scrollMyListViewToBottom();
                }
                //adapter.add(msg);
                // scrollMyListViewToBottom();
                //  if(!Constants.IsWeather)

                //{
                if(msg!=null) {
                    String sendMsg = CreatePayload.sendMessagePayload(msg);
                    mqttJava.sendStringMessage(MqttJava.getTopic(), sendMsg);
                }
                //  }

            }


        };
        receiver1 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String response = intent.getStringExtra("Type");
                // Boolean isLeft = intent.getBooleanExtra(Constants.COPA_STATE, true);
                //processResponse(response, isLeft);
                if(response.equals("subscribeTopic"))
                {
                    if(JsonParser.getCasandraMsgs()!=null) {
                        if (JsonParser.getCasandraMsgs().messages != null && JsonParser.getCasandraMsgs().messages.size() > 0) {
                            for (MessagesSubscribeTopic m : JsonParser.getCasandraMsgs().messages) {
                                adapter.add( convertToSendMessage(m));
                            }
                        }
                    }
                    mqttJava.subscribe(MqttJava.getTopic());
                    // String sendMsg = CreatePayload.sendMessagePayload(new SendMessage(true, MqttJava.getTopic(),"Globot", Constants.USERNAME,JsonParser.getCurrentTime(),1,"0"));
                    //  mqttJava.sendStringMessage(MqttJava.getTopic(), sendMsg);
                    grpName.setText(JsonParser.getSelectedGrp().groupName);
                    lv.setAdapter(adapter);
                    scrollMyListViewToBottom();
                }
                else if(response.equals("appLaunched"))
                {
                    mqttJava.subscribe("subscribeTopic");
                    mqttJava.sendStringMessage("subscribeTopic", "{'groupId':'" + JsonParser.getSelectedGrp().groupId + "'}");
                    grpName.setText(JsonParser.getSelectedGrp().groupName);
                }
                else if(response.equals("send_message")){
                    SendMessage msg = JsonParser.getMessageResponse();

                    // if(msg.userId!=null && !msg.userId.toString().equals(Constants.USERNAME)) {
                    adapter.add(msg);
                    if(msg.userId!=null && !msg.userId.toString().equals(Constants.USERNAME)) {
                        if (speaker.isAllowed())
                            voice(msg.msg);
                    }
                    //  mqttJava.sendStringMessage(MqttJava.getTopic(), sendMsg);
                    scrollMyListViewToBottom();
                    //}

                }
            }

        };
        lv = (ListView) findViewById(R.id.listView1);
        adapter = new MessageAdapter(getApplicationContext(), R.layout.listitem_discuss);
        editText1 = (EditText) findViewById(R.id.editText1);
        //text();

        fabanim = (FloatingActionButton) findViewById(R.id.fabanim);
        fabanim.setVisibility(View.VISIBLE);
        // fabanim.setShowProgressBackground(true);
        //fabanim.setIndeterminate(true);
        talk();
        // mProgressTypes.offer(ProgressType.INDETERMINATE);
        fabanim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressType type = mProgressTypes.poll();
                switch (type) {
                    case PROGRESS_NO_BACKGROUND:
                        if(!fabanim.isProgressBackgroundShown()){
                            fabanim.setShowProgressBackground(true);
                            fabanim.setIndeterminate(true);
                            talk();
                            mProgressTypes.offer(ProgressType.PROGRESS_NO_BACKGROUND);
                        }
                        else
                        {
                            if (isMyServiceRunning(MyService.class)) {
                                stopService(new Intent(MainActivity.this, MyService.class));
                            }
                            fabanim.hideProgress();
                            mProgressTypes.offer(ProgressType.INDETERMINATE);
                            fabanim.setShowProgressBackground(false);
                            fabanim.setIndeterminate(false);
                        }
                        break;
                    case INDETERMINATE:
                        if(!fabanim.isProgressBackgroundShown()){
                            fabanim.setShowProgressBackground(true);
                            fabanim.setIndeterminate(true);
                            talk();
                            mProgressTypes.offer(ProgressType.PROGRESS_NO_BACKGROUND);
                        }
                        else
                        {
                            if (isMyServiceRunning(MyService.class)) {
                                stopService(new Intent(MainActivity.this, MyService.class));
                            }
                            fabanim.hideProgress();
                            mProgressTypes.offer(ProgressType.INDETERMINATE);
                            fabanim.setShowProgressBackground(false);
                            fabanim.setIndeterminate(false);
                        }

                        break;
                }
            }
        });

        fabanim.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                menuRed.setVisibility(View.VISIBLE);
                menuRed.showMenuButton(true);
                menuRed.open(true);
                fabanim.setVisibility(View.GONE);
                return true;
            }
        });

        fabenter = (FloatingActionButton)findViewById(R.id.fabenter);
        fabenter.setVisibility(View.GONE);
        fabenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollMyListViewToBottom();
                if(!editText1.getText().toString().equals("")) {
                    if (editText1.getText().toString().toLowerCase().contains("gloria")) {
                        Constants.Globot = 1;
                        if (editText1.getText().toString().toLowerCase().contains("quite")) {
                            Constants.Globot = 0;
                        }
                    }
                    ArrayList<String> list = new ArrayList<String>();
                    list.add(editText1.getText().toString());
                    ArrayList<String> temp = reqrest.processHardCodeCommand(list);
                    SendMessage msg;
                    int i =0;
                    if(Constants.InList || Constants.IsWeather)
                    {
                        i=0;
                    }
                    else
                    {
                        i=Constants.Globot;
                    }

                    msg = new SendMessage(true, MqttJava.getTopic(), editText1.getText().toString(), Constants.USERNAME, JsonParser.getCurrentTime(), i, JsonParser.getState());
                    // adapter.add(msg);
                    //scrollMyListViewToBottom();
                    String sendMsg = CreatePayload.sendMessagePayload(msg);
                    mqttJava.sendStringMessage(MqttJava.getTopic(), sendMsg);
//request send
                    if(Constants.InList || Constants.IsWeather) {
                        Constants.InList=false;
                        String a = processResponse(temp.get(0), false);
                        if (a != null) {
                            msg = new SendMessage(true, MqttJava.getTopic(), a, "gloria", JsonParser.getCurrentTime(), 0, JsonParser.getState());
                            // adapter.add(msg);
                            // scrollMyListViewToBottom();
                            String sendMsg1 = CreatePayload.sendMessagePayload(msg);
                            mqttJava.sendStringMessage(MqttJava.getTopic(), sendMsg1);
                        }
                    }
                    editText1.setText("");
                }
            }
        });

        fabenter.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                menuRed.setVisibility(View.VISIBLE);
                menuRed.showMenuButton(true);
                menuRed.open(true);
                fabenter.setVisibility(View.GONE);
                fabanim.setVisibility(View.GONE);
                editText1.setVisibility(View.INVISIBLE);
                return true;
            }
        });
    }

    public SendMessage convertToSendMessage(MessagesSubscribeTopic msg)
    {
        SendMessage snd = new SendMessage(msg);
        return snd;
    }

    private void scrollMyListViewToBottom() {
        lv.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                lv.setSelection(adapter.getCount() - 1);
            }
        });
    }


    public String processResponse(String response, Boolean isRequest)
    {
        String a = "";
        String voiceresponse = null;
        // if (!isRequest) {
        if (response.toString().equals("Bye")) {
            voiceresponse = response;
            finish();
        } else if (Constants.quite) {
            Constants.quite = false;
            speaker.allow(false);
            voiceresponse = response;
        } else if (Constants.TALK) {
            Constants.TALK = false;
            speaker.allow(true);
            voiceresponse = response;
        } else if (Constants.isIOT) {
            Constants.isIOT = false;
            rblControls.checkBLESupport();
            rblControls.startBLE();
            if (!connectState) {
                voiceresponse = "I am looking for the switch, wait a second.";
                rblControls.scanLeDevice();

            } else {
                voiceresponse = response;
                rblControls.writeByte(Constants.IOT_NUMBER, Constants.IOT_SINGNAL);
            }
        }
        else if(response.toString().equals("how is the weather"))
        {
            voiceresponse = "location please?";

        }
        else if(Constants.IsWeather)
        {
            Constants.IsWeather=false;
            CityPreference city = new CityPreference(MainActivity.this);
            city.setCity(response);
            updateWeatherData(city.getCity());
        }
        else if(response.toString().equals("Please set time"))
        {
            voiceresponse = response;
            FragmentManager fm = getFragmentManager();
            AlarmActivity dialogFragment = new AlarmActivity();
            dialogFragment.show(fm, "Sample Fragment");
        }
        else {
            voiceresponse = response;
        }


        return voiceresponse;
    }
    String weather;
    private void updateWeatherData(final String city){
        new Thread(){
            public void run(){
                final JSONObject json = RemoteFetch.getJSON(MainActivity.this, city);
                if(json == null){
                    handler.post(new Runnable(){
                        public void run(){
                            weather = "Sorry, no weather data found.";
                            Toast.makeText(MainActivity.this,
                                    MainActivity.this.getString(R.string.place_not_found),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    handler.post(new Runnable(){
                        public void run(){
                            renderWeather(json);
                        }
                    });
                }
            }
        }.start();

    }

    private void renderWeather(JSONObject json){
        try {
            Constants.IsWeather = false;
            String City = (json.getString("name").toUpperCase(Locale.US) +
                    ", " +
                    json.getJSONObject("sys").getString("country"));

            JSONObject details = json.getJSONArray("weather").getJSONObject(0);
            JSONObject main = json.getJSONObject("main");
            String dataFields = (
                    details.getString("description").toUpperCase(Locale.US) +
                            "\n" + "Humidity: " + main.getString("humidity") + "%" +
                            "\n" + "Pressure: " + main.getString("pressure") + " hPa");

            String temperature = (
                    String.format("%.2f", main.getDouble("temp"))+ " ℃");
            weather = "Its "+temperature+ " "+dataFields;
            DateFormat df = DateFormat.getDateTimeInstance();
            String updatedOn = df.format(new Date(json.getLong("dt")*1000));
            String lastUpdated= "Last update: " + updatedOn;

            SendMessage msg;
            msg = new SendMessage(true, MqttJava.getTopic(), weather, "gloria", JsonParser.getCurrentTime(), 0 , JsonParser.getState());
            //adapter.add(msg);
            //scrollMyListViewToBottom();
            String sendMsg = CreatePayload.sendMessagePayload(msg);
            mqttJava.sendStringMessage(MqttJava.getTopic(), sendMsg);
         /*   setWeatherIcon(details.getInt("id"),
                    json.getJSONObject("sys").getLong("sunrise") * 1000,
                    json.getJSONObject("sys").getLong("sunset") * 1000);*/

        }catch(Exception e){
            Constants.IsWeather = false;
            SendMessage msg;
            msg = new SendMessage(true, MqttJava.getTopic(), "Sorry, no weather data found.", "gloria", JsonParser.getCurrentTime(), 0 , JsonParser.getState());
            String sendMsg = CreatePayload.sendMessagePayload(msg);
            mqttJava.sendStringMessage(MqttJava.getTopic(), sendMsg);
            Log.e("SimpleWeather", "One or more fields not found in the JSON data");
        }
    }

    Calendar c = Calendar.getInstance();
    public void talk()
    {
        isTextModeOn = false;
        editText1.setVisibility(View.INVISIBLE);
        fabanim.setShowProgressBackground(true);
        fabanim.setIndeterminate(true);
        mProgressTypes.offer(ProgressType.INDETERMINATE);
        startService(new Intent(MainActivity.this, MyService.class));
    }
    public void text()
    {
        isTextModeOn = true;
        editText1.setVisibility(View.VISIBLE);
        if(isMyServiceRunning(MyService.class)) {
            stopService(new Intent(MainActivity.this, MyService.class));
        }
    }
    public void voice(String message) {
        if(isMyServiceRunning(MyService.class)) {
            stopService(new Intent(MainActivity.this, MyService.class));
        }
        speaker.pause(Constants.LONG_DURATION);
        speaker.speak(message);
        speaker.pause(Constants.SHORT_DURATION);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mqttJava.backPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent("com.android.globot");
        sendBroadcast(intent);
    }
    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(Constants.COPA_RESULT)
        );
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver1),
                new IntentFilter(Constants.MQTT_MSG)
        );
        registerReceiver(mGattUpdateReceiver, RBLControls.makeGattUpdateIntentFilter());
    }

    @Override
    protected void onStop() {
        if(speaker!=null)
            speaker.destroy();
        if(rblControls!=null && isRBLActivated) {
            rblControls.disconnectService();
        }
        if(isMyServiceRunning(MyService.class)) {
            stopService(new Intent(MainActivity.this, MyService.class));
        }
        connectState = false;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver1);
        unregisterReceiver(mGattUpdateReceiver);
        super.onStop();
    }

    //IOT

    private static final int REQUEST_ENABLE_BT = 1;


    public static boolean connectState=false;
    private boolean isRBLActivated;
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (RBLService.ACTION_GATT_DISCONNECTED.equals(action)) {
                Toast.makeText(getApplicationContext(), "Disconnected",
                        Toast.LENGTH_SHORT).show();
                connectState = false;
            } else if (RBLService.ACTION_GATT_SERVICES_DISCOVERED
                    .equals(action)) {
                connectState = true;
                Log.d("BLE:::", "connected");
                rblControls.getCharactesticConnected();
                rblControls.writeByte(Constants.IOT_NUMBER, Constants.IOT_SINGNAL);
                Toast.makeText(getApplicationContext(), "Connected",
                        Toast.LENGTH_SHORT).show();
                isRBLActivated=true;
            } else if (RBLService.ACTION_DATA_AVAILABLE.equals(action)) {
                //data = intent.getByteArrayExtra(RBLService.EXTRA_DATA);
                //readAnalogInValue(data);
            } else if (RBLService.ACTION_GATT_RSSI.equals(action)) {
                //displayData(intent.getStringExtra(RBLService.EXTRA_DATA));
            }
        }
    };



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT){
            if(resultCode == Activity.RESULT_CANCELED) {
                return;
            }
            else if(resultCode == Activity.RESULT_OK)
            {
                rblControls.scanLeDevice();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Constants.isIOT) {
            registerReceiver(mGattUpdateReceiver, RBLControls.makeGattUpdateIntentFilter());
        }//isIOT
        //menuRed.setVisibility(View.VISIBLE);
    }

    private enum ProgressType {
        PROGRESS_NO_BACKGROUND,INDETERMINATE,
    }

    public View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean isGone = true;
            switch (v.getId()) {
                case R.id.fab1://speak
                    talk();
                    fabenter.setVisibility(View.GONE);
                    fabanim.setVisibility(View.VISIBLE);
                    break;
                case R.id.fab2://creategroup
                    // CreatePayload.createGroup();
                    Intent i =new Intent(MainActivity.this, MeetingActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    break;
                case R.id.fab3://text
                    text();
                    fabenter.setVisibility(View.VISIBLE);
                    fabanim.setVisibility(View.GONE);
                    break;
                case R.id.fab4://logout
                    isGone = true;
                    SharedPreferences.Editor editor = getSharedPreferences("USER", MODE_PRIVATE).edit();
                    editor.clear();
                    finish();
                    break;
                case R.id.fab5://participants
                    isGone = false;
                    participantsDialog();
                    Toast.makeText(MainActivity.this, JsonParser.getSelectedGrp().groupMembers.toString(), Toast.LENGTH_SHORT).show();
                    break;
            }
            menuRed.close(true);
            if(isGone)
                menuRed.setVisibility(View.GONE);
        }
    };
    public void participantsDialog()
    {
        FragmentManager fm = getFragmentManager();
        Dialog dialogFragment = new Dialog();
        dialogFragment.show(fm, "Sample Fragment");
    }

}

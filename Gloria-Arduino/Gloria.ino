
#include <ble_mini.h>
#define DIGITAL_OUT_PIN1   4
#define DIGITAL_OUT_PIN2   5
#define DIGITAL_OUT_PIN3   6

unsigned long currentMillis;        // store the current value from millis()
unsigned long previousMillis;       // for comparison with currentMillis
int samplingInterval = 250;          // how often to run the main loop (in ms)

void setup()
{
  BLEMini_begin(57600);
  
  pinMode(DIGITAL_OUT_PIN1, OUTPUT);
  pinMode(DIGITAL_OUT_PIN2, OUTPUT);
  pinMode(DIGITAL_OUT_PIN3, OUTPUT);
  // Default to internally pull high, change it if you need
  //digitalWrite(DIGITAL_IN_PIN, HIGH);
  //digitalWrite(DIGITAL_IN_PIN, LOW);
  
}

void loop()
{
  static boolean analog_enabled = false;
  static byte old_state = LOW;
  
  // If data is ready
  while ( BLEMini_available() == 3 )
  {
    // read out command and data
    byte data0 = BLEMini_read();
    byte data1 = BLEMini_read();
    byte data2 = BLEMini_read();
    
    if (data0 == 0x01)  // Command is to control digital out pin
    {
      if (data1 == 0x01)
        digitalWrite(DIGITAL_OUT_PIN1, HIGH);
      else
        digitalWrite(DIGITAL_OUT_PIN1, LOW);
    }
    else if (data0 == 0xA0) // Command is to enable analog in reading
    {
      if (data1 == 0x01)
        digitalWrite(DIGITAL_OUT_PIN2, HIGH);
      else
        digitalWrite(DIGITAL_OUT_PIN2, LOW);
    }
    else if (data0 == 0x02) // Command is to control PWM pin
    {
      if (data1 == 0x01)
        digitalWrite(DIGITAL_OUT_PIN3, HIGH);
      else
        digitalWrite(DIGITAL_OUT_PIN3, LOW);
      
      //analogWrite(PWM_PIN, data1);
    }
    else if (data0 == 0x04) // Command is to reset
    {
      digitalWrite(DIGITAL_OUT_PIN1, LOW);
      digitalWrite(DIGITAL_OUT_PIN2, LOW);
      digitalWrite(DIGITAL_OUT_PIN3, LOW);
    }
  }
  
  delay(100);
}


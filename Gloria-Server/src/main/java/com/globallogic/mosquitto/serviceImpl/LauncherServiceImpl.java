package com.globallogic.mosquitto.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globallogic.mosquitto.beans.Group;
import com.globallogic.mosquitto.beans.OnLaunchGroup;
import com.globallogic.mosquitto.beans.User;
import com.globallogic.mosquitto.dao.GroupDao;
import com.globallogic.mosquitto.dao.UserDao;
import com.globallogic.mosquitto.service.LauncherService;
import com.google.gson.Gson;

@Service
public class LauncherServiceImpl implements LauncherService{

	@Autowired
	UserDao userDao;
	@Autowired
	GroupDao groupDao;
	
	private static final Gson gson = new Gson();
	@Override
	public List<String> getUserIds() {
		// TODO Auto-generated method stub
		List<String> userIds = new ArrayList<String>(0);
		List<User> users = new ArrayList<User>(0);
		users = userDao.getAllUsers();
		for(User user: users){
			userIds.add(user.getUserId());
		}
		return userIds;
	}

	@Override
	public Set<String> getGroupsByUserId(String userId) {
		// TODO Auto-generated method stub
		User user = userDao.getUserByUserId(userId);
		return user.getGroupIds();
	}

	@Override
	public String getAppLauncherData(String userId) {
		// TODO Auto-generated method stub
		List<String> userIds = getUserIds();
		Set<String> groupIds = (Set<String>)getGroupsByUserId(userId);
		Group group;
		OnLaunchGroup onLaunchGroup = new OnLaunchGroup();
		onLaunchGroup.setUserIds(userIds.toArray(new String[userIds.size()]));
		for(String groupId:groupIds){
			group = new Group();
			group = groupDao.getGroupByGroupId(groupId);
			onLaunchGroup.addGroup(group);
		}
		String launcher = gson.toJson(onLaunchGroup, OnLaunchGroup.class);
		return launcher;
	}
	
}

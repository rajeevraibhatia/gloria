package com.globallogic.mosquitto.beans;

import org.springframework.cassandra.core.Ordering;
import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

@Table(value = "Message")
public class Message {

	@Column
	private String groupId;
	@Column
	private String messagePayload;
	@Column
	private String userId;
	@PrimaryKeyColumn(name = "time", type = PrimaryKeyType.PARTITIONED, ordinal = 1, ordering = Ordering.ASCENDING)
	private long time;

	public Message(String groupId, String messagePayload, String userId,
			long time) {
		this.groupId = groupId;
		this.messagePayload = messagePayload;
		this.userId = userId;
		this.time = time;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getMessagePayload() {
		return messagePayload;
	}

	public void setMessagePayload(String messagePayload) {
		this.messagePayload = messagePayload;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

}

package com.globallogic.mosquitto.daoImpl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.globallogic.mosquitto.beans.User;
import com.globallogic.mosquitto.cassandra.repository.UserRepository;
import com.globallogic.mosquitto.dao.UserDao;

@Repository
public class UserDaoImpl implements UserDao{

	@Autowired
	UserRepository userRepository;
	
	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return userRepository.getUsers();
	}

	@Override
	public Set<String> getUserGroups(String userId) {
		// TODO Auto-generated method stub
		return userRepository.getGroupsByUserId(userId);
	}

	@Override
	public User getUserByUserId(String userId) {
		// TODO Auto-generated method stub
		return userRepository.getUserByUserId(userId);
	}

	@Override
	public String getDeviceByUserId(String userId) {
		// TODO Auto-generated method stub
		return userRepository.getDeviceByUserId(userId);
	}

	@Override
	public User updateUser(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

}

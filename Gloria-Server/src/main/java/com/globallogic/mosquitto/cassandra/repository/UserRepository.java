package com.globallogic.mosquitto.cassandra.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import com.globallogic.mosquitto.beans.User;

@Repository
public interface UserRepository extends CassandraRepository<User>{
	
	@Query("SELECT * FROM User")
	public List<User> getUsers();
	
	@Query("SELECT groupIds FROM User WHERE userId = ?0 ALLOW FILTERING")
	public Set<String> getGroupsByUserId(String userId);
	
	@Query("SELECT * FROM User WHERE userId = ?0 ALLOW FILTERING")
	public User getUserByUserId(String user);

	@Query("SELECT deviceToken FROM User WHERE userId = ?0 ALLOW FILTERING")
	public String getDeviceByUserId(String userId);	
	
}

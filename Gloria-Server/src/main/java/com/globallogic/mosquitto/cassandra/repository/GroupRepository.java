package com.globallogic.mosquitto.cassandra.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import com.globallogic.mosquitto.beans.Group;

@Repository
public interface GroupRepository extends CassandraRepository<Group>{
	
	
	@Query("select * from group")
	public List<Group> getGroups();
	
	@Query("select * from group where groupId = ?0 ALLOW FILTERING")
	public Group getGroupByGroupId(String groupId);
	
	@Query("select members from group where groupId = ?0 ALLOW FILTERING")
	public Set<String> getUsersByGroupId(String groupId);	

	@Query("select groupName from group where groupId = ?0 ALLOW FILTERING")
	public String getGroupName(String groupId);		
	
}
 
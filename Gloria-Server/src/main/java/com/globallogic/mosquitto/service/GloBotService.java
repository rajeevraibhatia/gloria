package com.globallogic.mosquitto.service;

import com.globallogic.mosquitto.beans.GlobotMessagePayload;
import com.globallogic.mosquitto.beans.User;

public interface GloBotService {
	public String getBotResponse(GlobotMessagePayload globotMessagePayload);
	public String createUser(User user);
}
